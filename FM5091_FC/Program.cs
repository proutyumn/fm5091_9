﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace FM5091_FC
{
    class Program
    {
        static void Main(string[] args)
        {
            //IfThenStatement();
            //SwitchStatement();
            //ForStatement();
            //WhileAndDoStatements();
            //Console.WriteLine(Divide(19d, 5d));
            //Demo4();
            //AnimateDemo4();

            ChrisClass c = GetStuffFromChris();

            Console.WriteLine(c.FirstVar);
            Console.WriteLine(c.SecondVar);
            Console.WriteLine(c.ThirdVar);

            Console.ReadLine();
            
            // here are some changes to demonstrate the use of Git and Bitbucket
        }

        /// <summary>
        /// This method demonstrates the use of an if statement
        /// </summary>
        static void IfThenStatement()
        {
            // initialize the variable A
            int A = 44410;
            // demonstrate an if statement w/o curly brackets
            if(A == 50)
                Console.WriteLine("A is equal to 50!");
            // else if demo
            if(A == 5)
            {
                Console.WriteLine("A is equal to 5!");
            }
            else if (A == 10)
            {
                Console.WriteLine("A is equal to 10!");
            }
            else if (A < 0)
            {
                Console.WriteLine("A is a negative number!");
            }
            else
            {
                Console.WriteLine("I don't know what number A is...");
            }
        }

        /// <summary>
        /// This method demonstrates the use of a switch statement
        /// </summary>
        static void SwitchStatement()
        {
            // here i declare the variable A
            int A = 50;
            // here i use a switch statement to produce user output
            switch(A)
            {
                case 50:
                    Console.WriteLine("A is equal to 50!");
                    break;
                case 10:
                    Console.WriteLine("A is equal to 10!");
                    break;
                default:
                    Console.WriteLine("I don't know...");
                    break;
            }
        }

        /// <summary>
        /// This method demonstrate the use of a for loop
        /// </summary>
        static void ForStatement()
        {
            // here we declare the variable i *before and outside* of the for loop
            int i = 0;
            for (i = 0; i < 10; i++)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine();
            // here we declare the variable j *inside* the for loop
            for (int j = 0; j < 10; j++)
            {
                Console.Write(j + " ");
            }
            // the difference is the scope of the variables.
            // the scope of i is the method - the scope of j is the for loop.
            Console.WriteLine();
            Console.WriteLine();
            // here's a nested for loop.  it prints a matrix.
            for (int m = 0; m < 15; m++)
            {
                for (int n = 0; n < 15; n++)
                {
                    Console.Write(n + " ");
                }
                Console.WriteLine();
            }

        }

        /// <summary>
        /// This method demonstrates the use of a while and do loop
        /// </summary>
        static void WhileAndDoStatements()
        {
            int A = 0;
            // first we demonstrate the use of a while statement
            while(A > 0)
            {
                Console.WriteLine(A);
                A--;
            }
            Console.WriteLine();
            A = 0;
            // then we demo a do statement
            do
            {
                Console.WriteLine(A);
                A--;
            } while (A > 0);
            // the do loop executes code *at least once*
            // whereas the while loop checks the condition first.
        }

        /// <summary>
        /// This method takes two double objects and divides them, returning the quotient.
        /// </summary>
        /// <param name="a">A value of type double</param>
        /// <param name="b">A value of type double</param>
        /// <returns>The quotient of a / b</returns>
        static double Divide(double a, double b)
        {
            return a / b;
        }

        /// <summary>
        /// This method takes two double objects and divides them, returning the quotient.
        /// </summary>
        /// <param name="a">A value of type int</param>
        /// <param name="b">A value of type int</param>
        /// <returns>The quotient of a / b, in double</returns>
        static double Divide(int a, int b)
        {
            return (double)a / (double)b;
        }

        /// <summary>
        /// First C# demo from the lecture
        /// </summary>
        static void Demo1()
        {
            // remember there's a gotcha when dividing integers
            string txt;
            int X = 5, Y = 5;
            Console.WriteLine("X = 5 and Y = 5.  Enter 1 for X*Y and 2 for X/Y.");
            txt = Console.ReadLine();
            if(txt == "1")
                Console.WriteLine(X * Y);
            else
                Console.WriteLine(X / Y);
            Console.ReadLine();
        }

        /// <summary>
        /// Simple demo of a factorial calc
        /// </summary>
        static void Demo2()
        {
            string txt;
            int b = 1;
            Console.WriteLine("Enter a number to find the factorial!");
            txt = Console.ReadLine();
            for (int i = Convert.ToInt32(txt); i > 1; i--)
            {
                b *= i;
            }
            Console.WriteLine("Factorial is " + b);
        }

        /// <summary>
        /// Simple demo listing primes from 0 to 100
        /// </summary>
        static void Demo3()
        {
            bool notprime = false;
            for (int i = 2; i < 100; i++)
            {
                for (int j = i - 1; j > 1; j--)
                {
                    if (i % j == 0)
                    {
                        notprime = true;
                        break;
                    }
                }
                if (notprime == false)
                {
                    Console.WriteLine(i);
                }
                notprime = false;
            }
        }

        /// <summary>
        /// Draw a random matrix of X's on the screen
        /// </summary>
        static void Demo4()
        {
            Random rnd = new Random(); // instantiate the Random class as an object called "rnd"
            string txt = string.Empty;
            for (int i = 1; i < 21; i++)
            {
                for (int j = 1; j < 51; j++)
                {
                    if(rnd.NextDouble() < 0.5)
                    {
                        txt += " ";
                    }
                    else
                    {
                        txt += "X";
                    }
                }
                Console.WriteLine(txt);
                txt = "";
            }
        }

        /// <summary>
        /// Animate the drawing of random X matrices
        /// </summary>
        static void AnimateDemo4()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            for (int i = 0; i < 100; i++)
            {
                Console.Clear();
                Demo4();
                Thread.Sleep(500);
            }
        }

        static ChrisClass GetStuffFromChris()
        {
            ChrisClass c = new ChrisClass();
            c.FirstVar = 3;
            c.SecondVar = 5;
            c.ThirdVar = 19;
            return c;
        }
    }

    class ChrisClass
    {
        public int FirstVar = 0;
        public int SecondVar = 0;
        public int ThirdVar = 0;

        public void DoStuff()
        {
            // do stuff
        }
    }
}
